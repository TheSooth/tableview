//
//  ViewController.m
//  TableView
//
//  Created by TheSooth on 12/14/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "ViewController.h"
#import "TableViewDataSource.h"

@interface ViewController () <TableViewDataSourceDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) TableViewDataSource *dataSource;

@end


@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setupTableView];
}

- (void)setupTableView
{
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.tableView];
    
    self.dataSource = [[TableViewDataSource alloc] initWithTableView:self.tableView];
    self.dataSource.delegate = self;
}

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"DidSelectObject"
                                                        message:object
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    [alertView show];
    
}

@end
