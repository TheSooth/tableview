//
//  TableViewDataSource.m
//  TableView
//
//  Created by TheSooth on 12/14/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "TableViewDataSource.h"
#import "TableViewCell.h"
#import "TableViewHeader.h"

@interface TableViewDataSource ()

@property (nonatomic, strong) NSArray *objects;

@end

@implementation TableViewDataSource

- (instancetype)initWithTableView:(UITableView *)tableView
{
    NSParameterAssert(tableView);
    
    if (self = [super init]) {
        [self configure:tableView];
        self.objects = @[@"String1", @"String2", @"String3"];
    }
    
    return self;
}

- (void)configure:(UITableView *)tableView
{
    tableView.dataSource  = self;
    tableView.delegate = self;
    
    [tableView registerNib:[UINib nibWithNibName:@"TableViewCell" bundle:nil]
    forCellReuseIdentifier:NSStringFromClass([TableViewCell class])];
    
    [tableView registerClass:[TableViewHeader class] forHeaderFooterViewReuseIdentifier:NSStringFromClass([TableViewHeader class])];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 88.f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(didSelectObject:atIndexPath:)]) {
        [self.delegate didSelectObject:self.objects[indexPath.row] atIndexPath:indexPath];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    TableViewHeader *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([TableViewHeader class])];
    
    headerView.textLabel.text = @"Blalalalaal";
    headerView.detailTextLabel.text = @"Detailed text label";
    
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TableViewCell class])];
    [cell fillWithObject:self.objects[indexPath.row]];
    
    return cell;
}

@end
