//
//  TableViewDataSource.h
//  TableView
//
//  Created by TheSooth on 12/14/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TableViewDataSourceDelegate <NSObject>

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath;

@end

@interface TableViewDataSource : NSObject <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id <TableViewDataSourceDelegate> delegate;

- (instancetype)initWithTableView:(UITableView *)tableView;

@end
