//
//  TableViewCell.m
//  TableView
//
//  Created by TheSooth on 12/14/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation TableViewCell

@synthesize imageView;

- (void)fillWithObject:(id)object
{
    self.label.text = object;
    self.imageView.image = [UIImage imageNamed:@"rick_morty"];
}

- (void)prepareForReuse
{
    self.imageView.image = nil;
}

@end
