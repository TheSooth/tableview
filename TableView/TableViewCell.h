//
//  TableViewCell.h
//  TableView
//
//  Created by TheSooth on 12/14/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BaseTableViewCell <NSObject>

- (void)fillWithObject:(id)object;

@end

@interface TableViewCell : UITableViewCell <BaseTableViewCell>

@end
